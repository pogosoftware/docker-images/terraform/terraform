##########################################################################
### Builder
##########################################################################
FROM alpine:3.15.0 as builder
ARG VERSION=1.1.0

RUN apk add --no-cache curl=7.80.0-r0 git=2.34.1-r0 && \
    curl --fail --silent -L -o terraform.zip "https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_amd64.zip" && \
    unzip terraform.zip -d /usr/local/bin && \
    rm -rf terraform.zip && \
    rm -rf /var/cache/apk/*
